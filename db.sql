CREATE TABLE "address_object_items" (
    "id"	INTEGER NOT NULL,
    "object_id"	INTEGER NOT NULL,
    "change_id"	INTEGER NOT NULL,
    "level"	INTEGER NOT NULL,
    "oper_type_id"	INTEGER NOT NULL,
    "prev_id"	INTEGER,
    "next_id"	INTEGER,
    "object_guid"	uuid NOT NULL,
    "name"	varchar(512) NOT NULL,
    "type_name"	varchar(512) NOT NULL,
    "update_date"	TIMESTAMP NOT NULL,
    "start_date"	TIMESTAMP NOT NULL,
    "end_date"	TIMESTAMP NOT NULL,
    "is_actual"	BOOLEAN NOT NULL,
    "is_active"	BOOLEAN NOT NULL
);
CREATE TABLE "adm_hierarchy_items" (
   "id"	INTEGER NOT NULL,
   "object_id"	INTEGER NOT NULL,
   "parent_obj_id"	INTEGER,
   "region_code"	INTEGER NOT NULL,
   "prev_id"	INTEGER,
   "next_id"	INTEGER,
   "update_date"	TIMESTAMP NOT NULL,
   "start_date"	TIMESTAMP NOT NULL,
   "end_date"	TIMESTAMP NOT NULL,
   "is_active"	BOOLEAN NOT NULL,
   "path"	varchar(512) NOT NULL
);
CREATE TABLE "mun_hierarchy_items" (
   "id"	INTEGER NOT NULL,
   "object_id"	INTEGER NOT NULL,
   "parent_obj_id"	INTEGER,
   "change_id"	INTEGER NOT NULL,
   "prev_id"	INTEGER,
   "next_id"	INTEGER,
   "update_date"	TIMESTAMP NOT NULL,
   "start_date"	TIMESTAMP NOT NULL,
   "end_date"	TIMESTAMP NOT NULL,
   "is_active"	BOOLEAN NOT NULL,
   "path"	varchar(512) NOT NULL
);
CREATE TABLE "object_levels" (
    "level"	INTEGER NOT NULL,
    "start_date"	TIMESTAMP NOT NULL,
    "end_date"	TIMESTAMP NOT NULL,
    "update_date"	TIMESTAMP NOT NULL,
    "is_active"	BOOLEAN NOT NULL,
    "name"	varchar(512) NOT NULL
);
CREATE TABLE "param_types" (
    "id"	INTEGER NOT NULL,
    "name"	varchar(512) NOT NULL,
    "desc"	varchar(512) NOT NULL,
    "code"	varchar(512) NOT NULL,
    "is_active"	BOOLEAN NOT NULL,
    "update_date"	TIMESTAMP NOT NULL,
    "start_date"	TIMESTAMP NOT NULL,
    "end_date"	TIMESTAMP NOT NULL
);
CREATE TABLE "stead_items" (
    "id"	INTEGER NOT NULL,
    "object_id"	INTEGER NOT NULL,
    "change_id"	INTEGER NOT NULL,
    "oper_type_id"	INTEGER NOT NULL,
    "prev_id"	INTEGER,
    "next_id"	INTEGER,
    "update_date"	TIMESTAMP NOT NULL,
    "start_date"	TIMESTAMP NOT NULL,
    "end_date"	TIMESTAMP NOT NULL,
    "is_actual"	BOOLEAN NOT NULL,
    "is_active"	BOOLEAN NOT NULL,
    "object_guid"	varchar(512) NOT NULL,
    "number"	varchar(512)
);
