// Struct items.

use serde::Deserialize;

//AS_ADDR_OBJ_
#[derive(Debug, Deserialize, PartialEq)]
pub struct AddrObjectItem {
    #[serde(rename = "ID")]
    pub id: i32,
    #[serde(rename = "OBJECTID")]
    pub object_id: i32,
    #[serde(rename = "CHANGEID")]
    pub change_id: i32,
    #[serde(rename = "LEVEL")]
    pub level: i32,
    #[serde(rename = "OPERTYPEID")]
    pub oper_type_id: i32,
    #[serde(rename = "PREVID")]
    pub prev_id: Option<i32>,
    #[serde(rename = "NEXTID")]
    pub next_id: Option<i32>,
    #[serde(rename = "OBJECTGUID")]
    pub object_guid: uuid::Uuid,
    #[serde(rename = "NAME")]
    pub name: String,
    #[serde(rename = "TYPENAME")]
    pub type_name: String,
    #[serde(rename = "UPDATEDATE")]
    pub update_date: chrono::NaiveDate,
    #[serde(rename = "STARTDATE")]
    pub start_date: chrono::NaiveDate,
    #[serde(rename = "ENDDATE")]
    pub end_date: chrono::NaiveDate,
    #[serde(rename = "ISACTUAL")]
    pub is_actual: bool,
    #[serde(rename = "ISACTIVE")]
    pub is_active: bool,
}

// AS_ADM_HIERARCHY_
#[derive(Debug, Deserialize, PartialEq)]
pub struct AdmHierarchyItem {
    #[serde(rename = "ID")]
    pub id: i32,
    #[serde(rename = "OBJECTID")]
    pub object_id: i32,
    #[serde(rename = "CHANGEID")]
    pub change_id: i32,
    #[serde(rename = "REGIONCODE")]
    pub region_code: i32,
    #[serde(rename = "PARENTOBJID")]
    pub parent_obj_id: Option<i32>,
    #[serde(rename = "PREVID")]
    pub prev_id: Option<i32>,
    #[serde(rename = "NEXTID")]
    pub next_id: Option<i32>,
    #[serde(rename = "UPDATEDATE")]
    pub update_date: chrono::NaiveDate,
    #[serde(rename = "STARTDATE")]
    pub start_date: chrono::NaiveDate,
    #[serde(rename = "ENDDATE")]
    pub end_date: chrono::NaiveDate,
    // pub end_date: String,
    #[serde(rename = "ISACTIVE")]
    pub is_active: bool,
    #[serde(rename = "PATH")]
    pub path: String,
}

// AS_MUN_HIERARCHY_
#[derive(Debug, Deserialize, PartialEq)]
pub struct MunHierarchyItem {
    #[serde(rename = "ID")]
    pub id: i32,
    #[serde(rename = "OBJECTID")]
    pub object_id: i32,
    #[serde(rename = "CHANGEID")]
    pub change_id: i32,
    #[serde(rename = "PREVID")]
    pub prev_id: Option<i32>,
    #[serde(rename = "PARENTOBJID")]
    pub parent_obj_id: Option<i32>,
    #[serde(rename = "NEXTID")]
    pub next_id: Option<i32>,
    #[serde(rename = "UPDATEDATE")]
    pub update_date: chrono::NaiveDate,
    #[serde(rename = "STARTDATE")]
    pub start_date: chrono::NaiveDate,
    #[serde(rename = "ENDDATE")]
    pub end_date: chrono::NaiveDate,
    #[serde(rename = "ISACTIVE")]
    pub is_active: bool,
    #[serde(rename = "PATH")]
    pub path: String,
}

// AS_STEADS_
#[derive(Debug, Deserialize, PartialEq)]
pub struct SteadItem {
    #[serde(rename = "ID")]
    pub id: i32,
    #[serde(rename = "OBJECTID")]
    pub object_id: i32,
    #[serde(rename = "CHANGEID")]
    pub change_id: i32,
    #[serde(rename = "OPERTYPEID")]
    pub oper_type_id: i32,
    #[serde(rename = "PREVID")]
    pub prev_id: Option<i32>,
    #[serde(rename = "NEXTID")]
    pub next_id: Option<i32>,
    #[serde(rename = "UPDATEDATE")]
    pub update_date: chrono::NaiveDate,
    #[serde(rename = "STARTDATE")]
    pub start_date: chrono::NaiveDate,
    #[serde(rename = "ENDDATE")]
    pub end_date: chrono::NaiveDate,
    #[serde(rename = "ISACTUAL")]
    pub is_actual: bool,
    #[serde(rename = "ISACTIVE")]
    pub is_active: bool,
    #[serde(rename = "OBJECTGUID")]
    pub object_guid: uuid::Uuid,
    #[serde(rename = "NUMBER")]
    pub number: Option<String>, // NUMBER="62205/237"
}

// AS_OBJECT_LEVELS_
#[derive(Debug, Deserialize, PartialEq)]
pub struct ObjectLevel {
    #[serde(rename = "LEVEL")]
    pub level: i32,
    #[serde(rename = "STARTDATE")]
    pub start_date: chrono::NaiveDate,
    #[serde(rename = "ENDDATE")]
    pub end_date: chrono::NaiveDate,
    #[serde(rename = "UPDATEDATE")]
    pub update_date: chrono::NaiveDate,
    #[serde(rename = "ISACTIVE")]
    pub is_active: bool,
    #[serde(rename = "NAME")]
    pub name: String,
}

// AS_PARAM_TYPES_
#[derive(Debug, Deserialize, PartialEq)]
pub struct ParamType {
    #[serde(rename = "ID")]
    pub id: i32,
    #[serde(rename = "NAME")]
    pub name: String,
    #[serde(rename = "DESC")]
    pub desc: String,
    #[serde(rename = "CODE")]
    pub code: String,
    #[serde(rename = "ISACTIVE")]
    pub is_active: bool,
    #[serde(rename = "UPDATEDATE")]
    pub update_date: chrono::NaiveDate,
    #[serde(rename = "STARTDATE")]
    pub start_date: chrono::NaiveDate,
    #[serde(rename = "ENDDATE")]
    pub end_date: chrono::NaiveDate,
}
