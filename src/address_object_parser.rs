use std::path::PathBuf;

use serde::de::Deserialize;
use serde_xml_rs::from_reader;

use crate::address_objects::*;

#[derive(Debug)]
pub enum ParsingError {
    Deserialization,
    Io,
    NotImplemented,
    BadFileName,
}

pub fn auto_parser(f_path: &PathBuf) -> Result<EsiaWrapper, ParsingError> {
    match f_path.file_name().map(|n| n.to_str()) {
        Some(Some(name)) => {
            if name.starts_with("AS_ADDR_OBJ_") {
                return parse_file::<AddrObjectItems>(f_path).map(EsiaWrapper::AddrObjectItems);
            }
            if name.starts_with("AS_ADM_HIERARCHY_") {
                return parse_file::<AdmHierarchyItems>(f_path).map(EsiaWrapper::AdmHierarchyItems);
            }
            if name.starts_with("AS_MUN_HIERARCHY_") {
                return parse_file::<MunHierarchyItems>(f_path).map(EsiaWrapper::MunHierarchyItems);
            }
            if name.starts_with("AS_STEADS_") {
                return parse_file::<SteadItems>(f_path).map(EsiaWrapper::SteadItems);
            }
            if name.starts_with("AS_OBJECT_LEVELS_") {
                return parse_file::<ObjectLevels>(f_path).map(EsiaWrapper::ObjectLevels);
            }
            if name.starts_with("AS_PARAM_TYPES_") {
                return parse_file::<ParamTypes>(f_path).map(EsiaWrapper::ParamTypes);
            }

            Err(ParsingError::NotImplemented)
        }
        _ => Err(ParsingError::BadFileName),
    }
}

#[inline]
fn parse_file<'de, T: Deserialize<'de> + std::fmt::Debug>(
    f_path: &PathBuf,
) -> Result<T, ParsingError> {
    match std::fs::File::open(f_path.clone()) {
        Ok(o) => from_reader(o).map_err(|e| {
            log::warn!("Parsing error {f_path:?}: {e:#}");
            ParsingError::Deserialization
        }),
        Err(e) => {
            log::info!("Cannot read file {f_path:?}: {e}");
            Err(ParsingError::Io)
        }
    }
}
