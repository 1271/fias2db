use serde::Deserialize;

use crate::address_object_items::*;

#[derive(Debug)]
pub enum EsiaWrapper {
    AddrObjectItems(AddrObjectItems),
    AdmHierarchyItems(AdmHierarchyItems),
    MunHierarchyItems(MunHierarchyItems),
    SteadItems(SteadItems),
    ObjectLevels(ObjectLevels),
    ParamTypes(ParamTypes),
}

//AS_ADDR_OBJ_
#[derive(Debug, Deserialize, PartialEq)]
pub struct AddrObjectItems {
    #[serde(rename = "$value")]
    pub items: Vec<AddrObjectItem>,
}

// AS_ADM_HIERARCHY_
#[derive(Debug, Deserialize, PartialEq)]
pub struct AdmHierarchyItems {
    #[serde(rename = "$value")]
    pub items: Vec<AdmHierarchyItem>,
}

// AS_MUN_HIERARCHY_
#[derive(Debug, Deserialize, PartialEq)]
pub struct MunHierarchyItems {
    #[serde(rename = "$value")]
    pub items: Vec<MunHierarchyItem>,
}

// AS_STEADS_
#[derive(Debug, Deserialize, PartialEq)]
pub struct SteadItems {
    #[serde(rename = "$value")]
    pub items: Vec<SteadItem>,
}

// AS_OBJECT_LEVELS_
#[derive(Debug, Deserialize, PartialEq)]
pub struct ObjectLevels {
    #[serde(rename = "$value")]
    pub items: Vec<ObjectLevel>,
}

// AS_PARAM_TYPES_
#[derive(Debug, Deserialize, PartialEq)]
pub struct ParamTypes {
    #[serde(rename = "$value")]
    pub items: Vec<ParamType>,
}
