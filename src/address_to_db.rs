use crate::address_objects::*;
use crate::CFG;
use log::LevelFilter;
use sqlx::postgres::PgConnectOptions;
use sqlx::{Acquire, ConnectOptions};
use std::time::Duration;

type DbType = sqlx::postgres::Postgres;

#[derive(Clone)]
pub struct Inserter {
    db: sqlx::Pool<DbType>,
}

impl Inserter {
    pub async fn new() -> Self {
        // postgresql://localhost?dbname=mydb&user=postgres&password=postgres

        let slow_log_time = Duration::from_secs(CFG.read().unwrap().db_slow_log_time);
        let options = CFG
            .read()
            .unwrap()
            .db_connection
            .parse::<PgConnectOptions>()
            .expect("Cannot parse database url")
            .log_slow_statements(LevelFilter::Warn, slow_log_time);

        let conn = sqlx::Pool::<DbType>::connect_with(options)
            .await
            .expect("Cannot open db connection");

        Inserter { db: conn }
    }

    pub async fn insert_to_db(&self, item: EsiaWrapper) -> bool {
        match item {
            EsiaWrapper::AddrObjectItems(v) => self.insert_address_object_items(v).await,
            EsiaWrapper::AdmHierarchyItems(v) => self.insert_adm_hierarchy_items(v).await,
            EsiaWrapper::MunHierarchyItems(v) => self.insert_mun_hierarchy_items(v).await,
            EsiaWrapper::ObjectLevels(v) => self.insert_object_levels(v).await,
            EsiaWrapper::ParamTypes(v) => self.insert_param_types(v).await,
            EsiaWrapper::SteadItems(v) => self.insert_stead_items(v).await,
        }
    }

    pub async fn close(&self) {
        self.db.close().await;
    }

    async fn insert_address_object_items(&self, items: AddrObjectItems) -> bool {
        let mut all_ok = true;

        let db = self.db.clone();
        let mut transaction = db.begin().await.expect("Cannot open transaction");
        let mut cnt: usize = 0;

        for item in items.items {
            match sqlx::query(
                r#"INSERT INTO "address_object_items"
                (
                    id,object_id,change_id,level,oper_type_id,
                    prev_id,next_id,object_guid,name,type_name,
                    update_date,start_date,end_date,is_actual,is_active
                )
                VALUES (
                    $1,$2,$3,$4,$5,
                    $6,$7,$8,$9,$10,
                    $11,$12,$13,$14,$15
                )"#,
            )
            .bind(item.id)
            .bind(item.object_id)
            .bind(item.change_id)
            .bind(item.level)
            .bind(item.oper_type_id)
            .bind(item.prev_id)
            .bind(item.next_id)
            .bind(item.object_guid)
            .bind(item.name)
            .bind(item.type_name)
            .bind(item.update_date)
            .bind(item.start_date)
            .bind(item.end_date)
            .bind(item.is_actual)
            .bind(item.is_active)
            // .persistent(false)
            .execute(
                transaction
                    .acquire()
                    .await
                    .expect("Cannot open transaction from pool"),
            )
            .await
            {
                Ok(_) => {}
                Err(e) => {
                    log::error!("Error inserting item: {e}");
                    all_ok = false;

                    break;
                }
            }
            cnt += 1;
        }

        self.commit_or_rollback("address_object_items", all_ok, cnt, transaction)
            .await
    }

    async fn insert_adm_hierarchy_items(&self, items: AdmHierarchyItems) -> bool {
        let mut all_ok = true;

        let db = self.db.clone();
        let mut transaction = db.begin().await.expect("Cannot open transaction");
        let mut cnt: usize = 0;

        for item in items.items {
            match sqlx::query(
                r#"INSERT INTO "adm_hierarchy_items"
                (
                    id, object_id, parent_obj_id, region_code,
                    prev_id, next_id, update_date, start_date,
                    end_date, is_active, path
                ) VALUES (
                    $1, $2, $3, $4,
                    $5, $6, $7, $8,
                    $9, $10, $11
                )"#,
            )
            .bind(item.id)
            .bind(item.object_id)
            .bind(item.parent_obj_id)
            .bind(item.region_code)
            .bind(item.prev_id)
            .bind(item.next_id)
            .bind(item.update_date)
            .bind(item.start_date)
            .bind(item.end_date)
            .bind(item.is_active)
            .bind(item.path)
            .execute(
                transaction
                    .acquire()
                    .await
                    .expect("Cannot open transaction from pool"),
            )
            .await
            {
                Ok(_) => {}
                Err(e) => {
                    log::error!("Error inserting item: {e}");
                    all_ok = false;
                }
            };
            cnt += 1;
        }

        self.commit_or_rollback("adm_hierarchy_items", all_ok, cnt, transaction)
            .await
    }

    async fn insert_mun_hierarchy_items(&self, items: MunHierarchyItems) -> bool {
        let mut all_ok = true;

        let db = self.db.clone();
        let mut transaction = db.begin().await.expect("Cannot open transaction");
        let mut cnt: usize = 0;

        for item in items.items {
            match sqlx::query(
                r#"INSERT INTO "mun_hierarchy_items"
                (
                    id, object_id, parent_obj_id, change_id,
                    prev_id, next_id, update_date, start_date,
                    end_date, is_active, path
                ) VALUES (
                    $1, $2, $3, $4,
                    $5, $6, $7, $8,
                    $9, $10, $11
                )"#,
            )
            .bind(item.id)
            .bind(item.object_id)
            .bind(item.parent_obj_id)
            .bind(item.change_id)
            .bind(item.prev_id)
            .bind(item.next_id)
            .bind(item.update_date)
            .bind(item.start_date)
            .bind(item.end_date)
            .bind(item.is_active)
            .bind(item.path)
            .execute(
                transaction
                    .acquire()
                    .await
                    .expect("Cannot open transaction from pool"),
            )
            .await
            {
                Ok(_) => {}
                Err(e) => {
                    log::error!("Error inserting item: {e}");
                    all_ok = false;
                }
            };
            cnt += 1;
        }

        self.commit_or_rollback("mun_hierarchy_items", all_ok, cnt, transaction)
            .await
    }

    async fn insert_stead_items(&self, items: SteadItems) -> bool {
        let mut all_ok = true;

        let db = self.db.clone();
        let mut transaction = db.begin().await.expect("Cannot open transaction");
        let mut cnt: usize = 0;

        for item in items.items {
            match sqlx::query(
                r#"INSERT INTO "stead_items"
                (
                    id, object_id, change_id, oper_type_id,
                    prev_id, next_id, update_date, start_date,
                    end_date, is_actual, is_active, object_guid, number
                ) VALUES (
                    $1, $2, $3, $4,
                    $5, $6, $7, $8,
                    $9, $10, $11, $12, $13
                )"#,
            )
            .bind(item.id)
            .bind(item.object_id)
            .bind(item.change_id)
            .bind(item.oper_type_id)
            .bind(item.prev_id)
            .bind(item.next_id)
            .bind(item.update_date)
            .bind(item.start_date)
            .bind(item.end_date)
            .bind(item.is_actual)
            .bind(item.is_active)
            .bind(item.object_guid)
            .bind(item.number)
            .execute(
                transaction
                    .acquire()
                    .await
                    .expect("Cannot open transaction from pool"),
            )
            .await
            {
                Ok(_) => {}
                Err(e) => {
                    log::error!("Error inserting item: {e}");
                    all_ok = false;
                }
            };
            cnt += 1;
        }

        self.commit_or_rollback("stead_items", all_ok, cnt, transaction)
            .await
    }

    async fn insert_object_levels(&self, items: ObjectLevels) -> bool {
        let mut all_ok = true;

        let db = self.db.clone();
        let mut transaction = db.begin().await.expect("Cannot open transaction");
        let mut cnt: usize = 0;

        for item in items.items {
            match sqlx::query(
                r#"INSERT INTO "object_levels"
                (
                    level, start_date, end_date,
                    update_date, is_active, name
                ) VALUES (
                    $1, $2, $3,
                    $4, $5, $6
                )"#,
            )
            .bind(item.level)
            .bind(item.start_date)
            .bind(item.end_date)
            .bind(item.update_date)
            .bind(item.is_active)
            .bind(item.name)
            .execute(
                transaction
                    .acquire()
                    .await
                    .expect("Cannot open transaction from pool"),
            )
            .await
            {
                Ok(_) => {}
                Err(e) => {
                    log::error!("Error inserting item: {e}");
                    all_ok = false;
                }
            };
            cnt += 1;
        }

        self.commit_or_rollback("object_levels", all_ok, cnt, transaction)
            .await
    }

    async fn insert_param_types(&self, items: ParamTypes) -> bool {
        let mut all_ok = true;

        let db = self.db.clone();
        let mut transaction = db.begin().await.expect("Cannot open transaction");
        let mut cnt: usize = 0;

        for item in items.items {
            match sqlx::query(
                r#"INSERT INTO "param_types"
                (
                    id, name, "desc", code,
                    is_active, update_date, start_date, end_date
                ) VALUES (
                    $1, $2, $3, $4,
                    $5, $6, $7, $8
                )"#,
            )
            .bind(item.id)
            .bind(item.name)
            .bind(item.desc)
            .bind(item.code)
            .bind(item.is_active)
            .bind(item.update_date)
            .bind(item.start_date)
            .bind(item.end_date)
            .execute(
                transaction
                    .acquire()
                    .await
                    .expect("Cannot open transaction from pool"),
            )
            .await
            {
                Ok(_) => {}
                Err(e) => {
                    log::error!("Error inserting item: {e}");
                    all_ok = false;
                }
            };
            cnt += 1;
        }

        self.commit_or_rollback("param_types", all_ok, cnt, transaction)
            .await
    }

    #[inline]
    async fn commit_or_rollback(
        &self,
        table: &str,
        all_ok: bool,
        cnt: usize,
        transaction: sqlx::Transaction<'_, DbType>,
    ) -> bool {
        if all_ok {
            match transaction.commit().await {
                Ok(()) => {
                    log::info!("Successfully inserted {cnt} items to {table}");
                    true
                }
                Err(e) => {
                    log::error!(r#"Error commit transaction to {table}: {e} "#);
                    false
                }
            }
        } else {
            match transaction.rollback().await {
                Ok(()) => {
                    log::warn!("Error inserting items. Rollback");
                }
                Err(e) => {
                    log::error!("Error rollback: {e}");
                }
            }
            false
        }
    }
}
