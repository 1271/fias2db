use std::env;
use std::fs::read_dir;
use std::fs::read_to_string;
use std::path::PathBuf;
use std::sync::RwLock;

use lazy_static::lazy_static;

use crate::utils::DurationFmt;

mod address_object_items;
mod address_object_parser;
mod address_objects;
mod address_to_db;
mod task_loop;
mod utils;
#[cfg(test)]
mod tests;

type ReType = Vec<regex::Regex>;

// See also address_object_parser::auto_parser

lazy_static! {
    static ref RE_REGION_FILES: ReType = vec![
        regex::Regex::new(r"^AS_ADDR_OBJ_\d").unwrap(),
        regex::Regex::new(r"^AS_ADM_HIERARCHY_\d").unwrap(),
        regex::Regex::new(r"^AS_MUN_HIERARCHY_\d").unwrap(),
        regex::Regex::new(r"^AS_STEADS_\d").unwrap(),
    ];
    static ref RE_TOP_FILES: ReType = vec![
        regex::Regex::new(r"^AS_OBJECT_LEVELS_\d").unwrap(),
        regex::Regex::new(r"^AS_PARAM_TYPES_\d").unwrap(),
    ];
    pub static ref CFG: RwLock<AppConfig> = RwLock::new(Default::default());
}

fn main() {
    env_logger::init();

    match get_cfg_path() {
        Ok(p) if !make_config(p.clone()) => return,
        Err(_) => return,
        _ => {},
    };

    let t_summary = chrono::Utc::now();

    let parallel_tasks = CFG.read().unwrap().concurrent_threads;

    if parallel_tasks == 0 || parallel_tasks > 30 {
        log::error!("Bad parallel_tasks value: {parallel_tasks} expect 1..30");
        return;
    }

    tokio::runtime::Builder::new_multi_thread()
        .worker_threads(parallel_tasks + 1)
        .enable_all()
        .build()
        .unwrap()
        .block_on(async move {
            let t = chrono::Utc::now();
            log::info!("Reading root directory files.");
            if !processing_root().await {
                log::error!("Cannot processing root directory");
                return;
            }
            log::info!(
                "Root directory processing is done in {}",
                (chrono::Utc::now() - t).s()
            );

            let t = chrono::Utc::now();
            log::info!("Reading regions directories.");
            processing_regions().await;
            log::info!(
                "Regions processing is done in {}",
                (chrono::Utc::now() - t).s()
            );
        });

    log::info!("Summary time: {}", (chrono::Utc::now() - t_summary).s());
}

async fn get_directory_files(path: &PathBuf, filter: ReType) -> Result<Vec<PathBuf>, std::io::Error> {
    get_path_files(&path).map(|o| {
        o
            .iter()
            .map(|i| i.clone())
            .filter(|p| file_name_filter(p.clone(), filter.clone()))
            .collect()
    })
}

async fn processing_root() -> bool {
    match get_directory_files(&CFG.read().unwrap().files_root.clone(), RE_TOP_FILES.clone()).await {
        Ok(files) => {
            task_loop::processing(files).await
        }
        Err(e) => {
            log::error!("Cannot read root directory: {e}");
            false
        }
    }
}

async fn region_files(path: &PathBuf) -> Vec<PathBuf> {
    let regions_iter = read_dir(path.clone()).expect("Cannot oped root directory");

    let mut files = vec![];

    for d in regions_iter {
        match d {
            Ok(entry) => {
                let d_path = entry.path();
                if !d_path.is_dir() {
                    continue;
                }
                files.append(get_directory_files(&d_path, RE_REGION_FILES.clone()).await
                    .unwrap_or_default().as_mut())
            }
            Err(e) => {
                log::warn!("Bad dir {e}")
            }
        }
    };

    files
}

async fn processing_regions() -> bool {
    let root_path = CFG.read().unwrap().files_root.clone();

    let files = region_files(&root_path).await;

    let files_len = files.len();

    if files_len == 0 {
        log::warn!("No region files found");
        false
    } else {
        log::info!("Found {} region files", files_len);

        task_loop::processing(files).await
    }
}

fn file_name_filter(entry: PathBuf, filter: ReType) -> bool {
    entry
        .file_name()
        .map(|n| n.to_str())
        .flatten()
        .map(|n| filter.iter().any(|r| r.is_match(n)))
        .unwrap_or(false)
}

fn get_cfg_path() -> Result<String, ()> {
    let args = env::args().collect::<Vec<String>>();

    if args.len() != 2 {
        log::error!(
            r#"Usage: {} "/path/to/config.toml""#,
            args.get(0).unwrap_or(&"fias2db".to_string())
        );
        return Err(());
    }

    let path = args.get(1).unwrap().to_string();

    if !PathBuf::from(path.clone()).is_file() || !path.ends_with(".toml") {
        log::error!("Config path is not a file or not .toml: {path}");
        return Err(());
    }

    Ok(path)
}

fn make_config(path: String) -> bool {
    log::info!("Using config: {path}");
    read_to_string(path)
        .map(|c| {
            log::debug!("Config loaded");
            match toml::from_str(&c).map(|c| {
                CFG.write().unwrap().clone_from(&c);
            }) {
                Ok(_) => true,
                Err(e) => {
                    log::error!("Cannot parse config: {e}");
                    false
                }
            }
        })
        .unwrap_or(false)
}

fn get_path_files(path: &PathBuf) -> Result<Vec<PathBuf>, std::io::Error> {
    read_dir(path).map(|f| {
        f.filter(|p| match p {
            Ok(o) => o.path().is_file(),
            _ => false,
        })
        .map(|p| p.unwrap().path())
        .collect()
    })
}

#[derive(Debug, Clone, Default, serde::Deserialize)]
pub struct AppConfig {
    pub db_connection: String,
    pub files_root: PathBuf,
    pub db_slow_log_time: u64,
    pub concurrent_threads: usize,
}
