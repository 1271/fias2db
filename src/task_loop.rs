use std::path::PathBuf;
use std::sync::Arc;

use tokio::sync::RwLock;

use crate::address_object_parser::auto_parser;
use crate::address_to_db::Inserter;
use crate::utils::DurationFmt;
use crate::CFG;

const MB_BYTES: f64 = 1048576.0;

// Parallel file parsing.
pub async fn processing(files: Vec<PathBuf>) -> bool {
    let rw = Arc::new(RwLock::new(files.clone()));

    let inserter = Inserter::new().await;

    let tasks = (0..CFG.read().unwrap().concurrent_threads)
        .into_iter()
        .map(|n| tokio::task::spawn(processing_task(rw.clone(), inserter.clone(), n)));

    futures_util::future::join_all(tasks).await;

    inserter.close().await;
    true // TODO
}

async fn processing_task(files: Arc<RwLock<Vec<PathBuf>>>, inserter: Inserter, thread: usize) {
    async fn _get_file(files: Arc<RwLock<Vec<PathBuf>>>) -> Option<PathBuf> {
        files.write().await.pop()
    }

    let mut tt = chrono::Utc::now();

    loop {
        let files_left = files.read().await.len();
        let file = match _get_file(files.clone()).await {
            Some(o) => o,
            _ => break,
        };

        let file_size = (file.metadata().map(|m| m.len()).unwrap_or(0) as f64) / MB_BYTES; // 1Mb

        let t_delay = chrono::Utc::now() - tt;

        let t = chrono::Utc::now();
        match auto_parser(&file) {
            Ok(o) => {
                let parsing_time = chrono::Utc::now() - t;
                log::info!(
                    "Thread {thread} -> {file:?} {} ({file_size:.2}Mb) ({files_left} files left. Delay: {})",
                    parsing_time.s(),
                    t_delay.s(),
                );

                if !inserter.clone().insert_to_db(o).await {
                    log::error!("Error inserting items. See logs for details");
                    break;
                }

                tt = chrono::Utc::now();
            }
            Err(e) => {
                log::warn!("{e:?}")
            }
        };
    }
}
