use std::fs::File;
use std::path::PathBuf;
use std::str::FromStr;

use serde_xml_rs::from_reader;

use crate::address_object_parser::auto_parser;
use crate::address_objects::*;

#[test]
fn test_as_addr_obj() {
    let path = PathBuf::from_str("var/fias_ok/01/AS_ADDR_OBJ_0.XML").unwrap();
    let file = File::open(&path).unwrap();
    let content: AddrObjectItems = from_reader(file).expect("Cannot parse AS_ADDR_OBJ_0.XML");
    let parser_content = auto_parser(&path).expect("Cannot parse AS_ADDR_OBJ_0.XML");

    match parser_content {
        EsiaWrapper::AddrObjectItems(v) => assert_eq!(v, content),
        _ => assert!(false)
    }

    assert_eq!(content.items.len(), 12);
}

#[test]
fn test_as_adm_hierarchy() {
    let path = PathBuf::from_str("var/fias_ok/01/AS_ADM_HIERARCHY_0.XML").unwrap();
    let file = File::open(&path).unwrap();
    let content: AdmHierarchyItems = from_reader(file).expect("Cannot parse AS_ADM_HIERARCHY_0.XML");
    let parser_content = auto_parser(&path).expect("Cannot parse AS_ADM_HIERARCHY_0.XML");

    match parser_content {
        EsiaWrapper::AdmHierarchyItems(v) => assert_eq!(v, content),
        _ => assert!(false)
    }

    assert_eq!(content.items.len(), 10);
}

#[test]
fn test_as_mun_hierarchy() {
    let path = PathBuf::from_str("var/fias_ok/01/AS_MUN_HIERARCHY_0.XML").unwrap();
    let file = File::open(&path).unwrap();
    let content: MunHierarchyItems = from_reader(file).expect("Cannot parse AS_MUN_HIERARCHY_0.XML");
    let parser_content = auto_parser(&path).expect("Cannot parse AS_MUN_HIERARCHY_0.XML");

    match parser_content {
        EsiaWrapper::MunHierarchyItems(v) => assert_eq!(v, content),
        _ => assert!(false)
    }

    assert_eq!(content.items.len(), 25);
}

#[test]
fn test_as_steads() {
    let path = PathBuf::from_str("var/fias_ok/01/AS_STEADS_0.XML").unwrap();
    let file = File::open(&path).unwrap();
    let content: SteadItems = from_reader(file).expect("Cannot parse AS_STEADS_0.XML");
    let parser_content = auto_parser(&path).expect("Cannot parse AS_STEADS_0.XML");

    match parser_content {
        EsiaWrapper::SteadItems(v) => assert_eq!(v, content),
        _ => assert!(false)
    }

    assert_eq!(content.items.len(), 12);
}

#[test]
fn test_as_object_levels() {
    let path = PathBuf::from_str("var/fias_ok/AS_OBJECT_LEVELS_0.XML").unwrap();
    let file = File::open(&path).unwrap();
    let content: ObjectLevels = from_reader(file).expect("Cannot parse AS_OBJECT_LEVELS_0.XML");
    let parser_content = auto_parser(&path).expect("Cannot parse AS_OBJECT_LEVELS_0.XML");

    match parser_content {
        EsiaWrapper::ObjectLevels(v) => assert_eq!(v, content),
        _ => assert!(false)
    }

    assert_eq!(content.items.len(), 17);
}

#[test]
fn test_as_param_types() {
    let path = PathBuf::from_str("var/fias_ok/AS_PARAM_TYPES_0.XML").unwrap();
    let file = File::open(&path).unwrap();
    let content: ParamTypes = from_reader(file).expect("Cannot parse AS_PARAM_TYPES_0.XML");
    let parser_content = auto_parser(&path).expect("Cannot parse AS_PARAM_TYPES_0.XML");

    match parser_content {
        EsiaWrapper::ParamTypes(v) => assert_eq!(v, content),
        _ => assert!(false)
    }

    assert_eq!(content.items.len(), 20);
}
