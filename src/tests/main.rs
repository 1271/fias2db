
use std::str::FromStr;

#[tokio::test]
async fn test_region_files() {
    let path = std::path::PathBuf::from_str("var/fias_ok").unwrap();
    let files = crate::region_files(&path).await;
    assert_eq!(files.len(), 4);
    assert!(!files.iter().any(|p| p.ends_with("AS_ADDR_OBJ_Bad-Name.XML")));
}

#[tokio::test]
async fn test_make_config() {
    assert!(crate::make_config("app_config.toml.dist".to_string()));
    assert_eq!(crate::CFG.read().unwrap().clone().db_connection, "postgresql://user:password@192.168.2.2:5432/db_name".to_string());
    assert_eq!(crate::CFG.read().unwrap().clone().concurrent_threads, 8);
    assert_eq!(crate::CFG.read().unwrap().clone().files_root, std::path::PathBuf::from_str("path/to/fias/files/").unwrap());
    assert_eq!(crate::CFG.read().unwrap().clone().db_slow_log_time, 5);
}