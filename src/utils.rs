pub trait DurationFmt {
    fn s(&self) -> String;
}

const DAY_SECONDS: i64 = 24 * 60 * 60;
const HOUR_SECONDS: i64 = 60 * 60;
const MINUTE_SECONDS: i64 = 60;

impl DurationFmt for chrono::TimeDelta {
    fn s(&self) -> String {
        let millis = self.subsec_nanos() / 10_000_000;
        let seconds_full = self.num_seconds();
        let days = seconds_full / DAY_SECONDS;
        let reminder = seconds_full - days * DAY_SECONDS;
        let hours = reminder / HOUR_SECONDS;
        let reminder = reminder - hours * HOUR_SECONDS;
        let minutes = reminder / MINUTE_SECONDS;
        let reminder = reminder - minutes * MINUTE_SECONDS;
        let seconds = reminder % 60;

        if days > 0 {
            format!("TS{days}d. {days}:{minutes}:{seconds}.{millis:>02}")
        } else {
            format!("TS{days}:{minutes}:{seconds}.{millis:>02}")
        }
    }
}
